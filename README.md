Fork of Suckless' DWM window manager with custom build. Custom builds live in `own` branch. `master` branch is used to pull from `upstream` repo (https://git.suckless.org/dwm)

The branch `experimental` is used to make experiments, play with configs, and if succeed, push it to `own` branch.

## `own` branch

Implemented features:

- changed Mod key to **Mod4** "super key"/"windows key"
- explicitly set font for "st"
- decreased tags to 5
- added shortcuts to start Firefox
- integrated `dwmblocks` for status monitor
- layout now applayed for each tag separately
- reverted back to dispaly all tags (do not hide vacant tag `hide vacant tags` as per patch)
- `clientindicators` patch was implemented together with `hidevacanttags` and left active 
