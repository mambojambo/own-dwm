# Made changes and comments

### Sep 21,2020: Added emoji support for DWM, ST, DMENU

Since these projects use **libXft** library to display glyphs/fonts, some modifications should be done.

First of all, **libXft** (at the moment of writting it is v2.3.3, not updates/patches were made more than a year; sadly ) has to be patched with BGRA glyphs display and scaling support since it has bugs re this functionality. There is a [patch](https://gitlab.freedesktop.org/xorg/lib/libxft/-/commit/b77e5752cbd4acef90904e00c0f392984c321ca9?merge_request_iid=1) and [merge request](https://gitlab.freedesktop.org/xorg/lib/libxft/-/merge_requests/1)  but no one merged it to the upstream and we are left on our boats alone. After applying the patch reinstall **libXft**.

Than some changes have to be made in DWM's sorce files, namely in the `drw.c` file the following code has to be commented out:

```C
        FcBool iscol;
        if(FcPatternGetBool(xfont->pattern, FC_COLOR, 0, &iscol) == FcResultMatch && iscol) {
                XftFontClose(drw->dpy, xfont);
                return NULL;
        }
```

The above change are made thanks to [Luke Smith's](https://lukesmith.xyz/) [comment](https://gitlab.freedesktop.org/xorg/lib/libxft/-/merge_requests/1?commit_id=b77e5752cbd4acef90904e00c0f392984c321ca9#note_438180)
